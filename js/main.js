var DSSV = "DSSV";
var dssv = [];

// lấy dữ liệu từ localStorage
//  object lấy từ localStorage sẽ bị mất các key chứ function ( method )
var dssvJson = localStorage.getItem(DSSV);
// convert json to array
if (dssvJson != null) {
  var svArr = JSON.parse(dssvJson);
  // convert array chứa object ko có key tinhDTB() thành array chứa object có key tinhDTB()
  dssv = svArr.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV(dssv);
}
// thêm sinh viên
function themSinhVien() {
  var sv = layThongTinTuForm();

  // validate
var isValid = true;
// Kiểm tra mã Sinh Viên
isValid= kiemTraTrung(sv.ma, dssv)&&kiemTraDoDai(sv.ma,"spanMaSV",4,8)&&kiemTraSo(sv.ma,"spanMaSV");

// Kiểm tra email
isValid=isValid&kiemTraEmail(sv.email);

if(isValid){
// nếu isValid true thì tiến hành thêm sv
  dssv.push(sv);
  // lưu vào localStorage ( không bị mất dữ liệu khi load trang )

  // convert array dssv thành json
  var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
  localStorage.setItem(DSSV, dssvJson);
  // render dssv
  renderDSSV(dssv);
}
}
// xoá sinh viên
function xoaSinhVien(idSv) {
  // tìm vị trí

  var viTri = timKiemViTri(idSv, dssv);

  if (viTri != -1) {
    dssv.splice(viTri, 1);
    // render lại layout sau khi xoá thành công
    renderDSSV(dssv);
  }
}

// sửa sv

function suaSinhVien(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  if (viTri == -1) {
    //  dừng chương trình nếu ko tìm thấy
    return;
  }
  var sv = dssv[viTri];
  // show thông tin lên form
  showThongTinLenForm(sv);
}

function capNhatSinhVien() {
  // lấy thông tin sau khi user update
  var sv = layThongTinTuForm();
  console.log(`  🚀: capNhatSinhVien -> sv`, sv);
  // update dữ diệu mới thay thế dữ liệu cũ
  var viTri = timKiemViTri(sv.ma, dssv);
  if (viTri != -1) {
    dssv[viTri] = sv;
    renderDSSV(dssv);
  }

  // render lại layout sau khi update
}
